﻿using Esphera.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;

namespace Esphera.Utils
{
    class Send
    {
        private List<Order> orders;
        private ServerResponse sResponse;
        private SendDataStore sds;

        private int storeId = 0;
        private string storeName = "";
        private string username = "";
        private string password = "";
        private bool environment;
        private string mall;

        public Send(List<Order> orders, int storeId, string storeName, string username, string password, bool environment, string mall)
        {
            // Corrige um erro que raramente acontece
            // O cliente inclui um header Expect: 100-Continue
            // E o servidor responde com 417 - Expectation Failed
            System.Net.ServicePointManager.Expect100Continue = false;

            // Set Orders
            this.orders = orders;
            this.storeId = storeId;
            this.storeName = storeName;
            this.username = username;
            this.password = password;
            this.environment = environment;
            this.mall = mall;

            // Object json
            sds = new SendDataStore(this.storeId,this.storeName,orders);
        }

        public void send()
        {
            Console.WriteLine(String.Format("Enviando {0} registro(s) de {1}",
                sds.getCurrentPosition() + sds.getIndex(), this.orders.Count));

            // Convert orders to string
            String contentOrders = JsonConvert.SerializeObject(sds.getData());

            // Get URI based environment
            String URI = "";

            if (this.environment)
                URI = String.Format(@"https://{0}.nappsolutions.com", mall);
            else
                URI = String.Format(@"http://{0}.sandbox.nappsolutions.com", mall);

            URI += "/service/receiver";

            // POST data
            var postData = "content=" + contentOrders;
            var data = Encoding.ASCII.GetBytes(postData);

            // Build request
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URI);
            request.Method = "POST";
            request.ContentLength = data.Length;
            request.ContentType = "application/x-www-form-urlencoded";

            // Request credentials
            request.Credentials = new NetworkCredential(this.username,this.password);

            // Write data to request
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(data, 0, data.Length);
            dataStream.Close();

            // Do Request
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            String rString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            // Parse response
            sResponse = JsonConvert.DeserializeObject<ServerResponse>(rString);
            sResponse.countProcessed(sResponse.total);

            // Show server response
            Console.WriteLine(sResponse + "\n");

            if (!sResponse.status)
                throw new Exception("O servidor respondeu com um erro: " +
                    sResponse.msg.Trim());

            if (sds.hasData())
                this.send();
        }

        public ServerResponse getResponse()
        {
            return sResponse;
        }
    }

    class SendDataStore
    {
        public int idLoja;
        public String nomeLoja;
        private List<Order> pedidosTuple;
        public List<Order> pedidos;
        private int index;

        public SendDataStore(int id, String name, List<Order> orders)
        {
            this.idLoja = id;
            this.nomeLoja = name;
            this.pedidosTuple = orders;
            this.index = 0;
        }

        public SendDataStore getData()
        {
            if (!hasData())
                return this;

            int position = getCurrentPosition();
            this.pedidos = pedidosTuple.GetRange(this.index, position);
            this.index += position;
            return this;
        }

        public int getIndex()
        {
            return index;
        }

        public int getCurrentPosition()
        {
            int total = 1000;
            if (index + total > pedidosTuple.Count - 1)
                total = pedidosTuple.Count - index;

            return total;
        }

        public bool hasData()
        {
            if (pedidosTuple.Count >= index + 1)
                return true;

            return false;
        }
    }

    class ServerResponse
    {
        public bool status;
        public String msg;
        public UInt32 total;
        private static uint sum = 0;

        public override string ToString()
        {
            String output = String.Format("{0}, {1} registro(s) novo(s)",
                    (status) ? "Ok" : "Fail", total); ;

            return output;
        }

        public void countProcessed(uint total)
        {
            sum += total;
        }

        public uint getProcessed()
        {
            return sum;
        }
    }
}
