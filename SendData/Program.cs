﻿using Esphera.Models;
using Esphera.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendData
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Order> orders = new List<Order>();

            //Venda 1
            Order order1 = new Order();
            order1.pedidoCode = "PedidoTeste01";
            order1.valorTotal = Convert.ToDecimal("80,50");
            order1.setDateTime(Convert.ToDateTime("17/08/2017"));
            orders.Add(order1);

            //Venda 2
            Order order2 = new Order();
            order2.pedidoCode = "PedidoTeste02";
            order2.valorTotal = Convert.ToDecimal("60,50");
            order2.setDateTime(Convert.ToDateTime("18/08/2017"));
            orders.Add(order2);

            //Venda 3
            Order order3 = new Order();
            order3.pedidoCode = "PedidoTeste03";
            order3.valorTotal = Convert.ToDecimal("73,50");
            order3.setDateTime(Convert.ToDateTime("19/08/2017"));
            orders.Add(order3);


            //Devolução 1
            Order order4 = new Order();
            order4.pedidoCode = "DevoluçãoTeste01";
            order4.valorTotal = Convert.ToDecimal("-75,50");
            order4.setDateTime(Convert.ToDateTime("19/08/2017"));
            orders.Add(order4);

            int lojaId = 681;
            string lojaNome = "Nome da Loja";
            string usuario = "Usuario Loja";
            string senha = "Senha Loja";
            bool ambiente = false;// True = Produção - False = Teste
            string rede = "Shopping Rede";

            Send send = new Send(orders, lojaId, lojaNome, usuario, senha, ambiente, rede);
            send.send();
        }
    }
}
