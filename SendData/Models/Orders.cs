﻿using System;
using System.Text;
using Newtonsoft.Json;
using System.Diagnostics;

namespace Esphera.Models
{
    public class Order
    {
        private int OrderId;
        private String OrderCode;
        private decimal OrderTotal;
        private DateTime OrderDate;
        private string OrderDoc;
        public string coo { get; set; }

        public int pedidoId
        {
            get
            {
                return this.OrderId;
            }
            set
            {
                OrderId = value;
            }
        }

        public String pedidoCode
        {
            get
            {
                return this.OrderCode;
            }
            set
            {
                OrderCode = value;
            }
        }

        public decimal valorTotal
        {
            get
            {
                return this.OrderTotal;
            }
            set
            {
                OrderTotal = value;
            }
        }

        public long dataPedido
        {
            get
            {
                var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                return Convert.ToInt64((this.OrderDate.ToUniversalTime() - epoch).TotalMilliseconds);
            }
        }

        public void setDateTime(DateTime dt)
        {
            this.OrderDate = dt;
        }

        public string documento
        {
            get
            {
                return this.OrderDoc;
            }
            set
            {
                if (value.Trim().Length >= 11)
                    OrderDoc = value.Trim();
            }
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            String nLine = Environment.NewLine;

            str.Append("Id: " + pedidoId + nLine);
            str.Append("Code: " + pedidoCode + nLine);
            str.Append("Total: " + valorTotal + nLine);
            str.Append("Date: " + dataPedido + nLine);
            str.Append("Documento: " + documento + nLine);

            return str.ToString();
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}